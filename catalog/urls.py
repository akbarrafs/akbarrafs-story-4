from django.urls import path
from . import views

app_name = "catalog"
urlpatterns = [
    path("", views.home, name="Home"),
    path("home/portofolio/", views.portofolio, name="Portofolio"),
    path("background/", views.background, name="Background"),
    path("resume/", views.resume, name="Resume"),
    path("contact/", views.contact, name="Contact"),
    path("contactlist/", views.contactlist, name="ContactList"),
    path("video/", views.video, name="Video"),
    path("gallery/", views.gallery, name="Gallery"),
    path("schedule/", views.jadwal, name="Schedule"),
    path ("/delete/<int:items_id>/", views.deleteItems, name="delete"),
]