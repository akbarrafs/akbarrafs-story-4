# Generated by Django 2.1.4 on 2019-10-05 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20191005_1650'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hasil',
            name='tanggal',
            field=models.DateField(default='Unknown', max_length=10),
        ),
        migrations.AlterField(
            model_name='hasil',
            name='waktu',
            field=models.TimeField(default='Unknown', max_length=10),
        ),
    ]
