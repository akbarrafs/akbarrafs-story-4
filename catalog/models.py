from django.db import models

# Create your models here.
class Hasil(models.Model):
    hari = models.CharField(max_length = 10, default="Unknown")
    tanggal = models.DateField(max_length = 10, default="Unknown")
    waktu = models.TimeField(max_length = 10, default="Unknown")
    kegiatan = models.CharField(max_length= 100, default="Unknown")
    lokasi = models.CharField(max_length = 20, default="Unknown")
    kategori = models.CharField(max_length = 20, default="Unknown")
