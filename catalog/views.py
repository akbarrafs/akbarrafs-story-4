from django.shortcuts import render, redirect
from .models import Hasil
from django.core.exceptions import ValidationError

def home(request):
    return render(request, "Home.html")

def portofolio(request):
    return render(request, "Portofolio.html")

def background(request):
    return render(request, "Background.html")

def resume(request):
    return render(request, "Resume.html")

def contact(request):
    return render(request, "Contact.html")

def contactlist(request):
    return render(request, "ContactList.html")

def gallery(request):
    return render(request, "Gallery.html")

def video(request):
    return render(request, "Video.html")

def deleteItems(request, items_id):
    a = Hasil.objects.get(id=items_id)
    a.delete()
    return redirect("catalog:Schedule")

def jadwal(request):
    context = {}
    if request.method == "POST":
        hari = request.POST.get('hari')
        tanggal = request.POST.get('tanggal')
        waktu = request.POST.get('waktu')
        kegiatan = request.POST.get('kegiatan')
        lokasi = request.POST.get('lokasi')
        kategori = request.POST.get('kategori')
        hasil = Hasil(hari=hari, tanggal=tanggal, waktu=waktu, kegiatan=kegiatan, lokasi=lokasi, kategori=kategori)
        try:
            hasil.clean()
            hasil.save()
        except ValidationError:
            pass
    context['hasilan'] = Hasil.objects.all().order_by('tanggal')    
    return render(request, "Jadwal.html", context)